require("dotenv").config({ path: __dirname + "/.env" });
const http = require('http');
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const middleware = require('./utils/middleware');
const apiRouter = require("./routes/api");

app.disable('x-powered-by');
app.use(bodyParser.json({}));
app.use("/", apiRouter);

const server = http.createServer(app);
server.listen(process.env["API_PORT"], () => {
  console.log('Gmailer API is started at port ' + process.env["API_PORT"]);
});

app.use(function (request, response, next) {
  const notFoundError = {
    code: 404,
    message: `Not found. Requested resource: ${request.method} ${request.originalUrl}`
  };
  middleware.processError(notFoundError, request, response, next);
});

app.use(middleware.processError);
