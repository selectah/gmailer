require('dotenv').config({path: __dirname + '/.env'});
const async = require('async');
const ObjectID = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const mongoURL = `mongodb://${process.env['MONGO_HOST']}:${process.env['MONGO_PORT']}/${process.env['MONGO_DB_NAME']}`;
const redis = require('redis');
const redisClient = redis.createClient(process.env['REDIS_PORT'], process.env['REDIS_HOST'], {db: process.env['REDIS_DB']});
const beeQueue = require('bee-queue');
const googleAPI = require('./modules/google-api');
const {google} = require('googleapis');

let oAuth2Client;
let mailQueue;
let mongoDB;

async.waterfall(
  [
    (done) => {
      googleAPI.authorize(done)
    },
    (auth, done) => {
      console.log('GMail API authorization client is ready.');
      oAuth2Client = auth;
      MongoClient.connect(mongoURL, {useUnifiedTopology: true}, (error, client) => {
        if(!error) {
          console.log('MongoDB is ready.');
          mongoDB = client.db(process.env['MONGO_DB_NAME']);
          done();
        } else {
          done('Failed to connect to MongoDB. ' + error.toString());
        }
      });
    },
    (done) => {
      mailQueue = new beeQueue(process.env['REDIS_QUEUE_NAME'],
        {
          redis: redisClient,
          isWorker: true,
          removeOnSuccess: true,
          removeOnFailure: true
        }
      );

      mailQueue.on('ready', () => {
        console.log(process.env['REDIS_QUEUE_NAME'] + ' redis queue is ready.');
        done();
      });

      mailQueue.on('error', (err) => {
        done(`${process.env['REDIS_QUEUE_NAME']} queue error: ${err.message}`);
      });
    },
  ],
  (error) => {
    if(error) {
      console.error(error);
      process.exit(-1);
    } else {
      processQueue();
    }
  }
);

function processQueue(){
  mailQueue.process((mail, complete) => {
    const emailBody = getEmailBody(mail.data.to, mail.data.from, mail.data.subject, mail.data.message);
    return sendMessage(emailBody, (error, response) => {
      complete(error, mail.data._id)
    });
  });

  mailQueue.on('job succeeded', (jobId, mailId) => {
    console.log(`Job ${jobId} succeeded with result: ${mailId}`);
    mongoDB.collection(process.env['MONGO_COLLECTION_NAME'])
      .updateOne(
        { '_id': new ObjectID(mailId)},
        {$set: {'status': 'sent'}},
        {upsert: false},
        (error) => {
          if(error) {
            console.error('MongoDB update error: ', error.toString());
          }
        }
      );
  });

  mailQueue.on('job retrying', (jobId, err) => {
    console.log(`Job ${jobId} failed with error ${err} but is being retried!`);
  });

  mailQueue.on('job failed', (jobId, err) => {
    console.error(`Job ${jobId} failed with error ${err.message}`);
    mongoDB.collection(process.env['MONGO_COLLECTION_NAME'])
      .updateOne(
        { '_id': new ObjectID(err.message)},
        {$set: {'status': 'failed'}},
        {upsert: false},
        (error) => {
          if(error) {
            console.error('MongoDB update error: ', error.toString());
          }
        }
      );
  });
}

function getEmailBody(to, from, subject, message) {
  const email = ["Content-Type: text/plain; charset=\"UTF-8\"\n",
    "MIME-Version: 1.0\n",
    "Content-Transfer-Encoding: 7bit\n",
    "to: ", to, "\n",
    "from: ", from, "\n",
    "subject: ", subject, "\n\n",
    message
  ].join('');

  const encodedMail = new Buffer(email).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
  return encodedMail;
}

function sendMessage(emailBody, complete) {
  const gmail = google.gmail({version: 'v1', oAuth2Client});
  gmail.users.messages.send({
      auth: oAuth2Client,
      userId: 'me',
      resource: {
        raw: emailBody
      }
    },
    complete
  );
}