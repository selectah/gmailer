const express = require("express"),
  router = express.Router(),
  middleware = require('../utils/middleware'),
  mailboxModule = require('../modules/mailbox');

router.post("/email", middleware.validateRequest, (request, response, next) => {
  try {
    mailboxModule.saveMail(request.body, (error, result) => {
      if (!error) {
        response.status(201).json({code: 201, message: 'Письмо добавлено в очередь рассылки.'});
      } else {
        next(error);
      }
    });
  }
  catch (error) {
    next(error);
  }
});

module.exports = router;
