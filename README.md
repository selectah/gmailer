# Gmailer
Пример использования Google API для отправления писем.

## Требования к системе
- nodejs
- mongodb
- redis

## Установка
```
git clone https://selectah@bitbucket.org/selectah/gmailer.git
cd gmailer
npm install
```

## Конфигурация

В [консоли Google API](https://console.developers.google.com/apis) необходимо создать проект и активировать Gmail API.
На странице [Учетные данные](https://console.developers.google.com/apis/credentials) создать учетные данные с типом **Приложение для ПК**. 
Скачать учетные данные. В сохраненном файле - поля **client_id**, **client_secret**, **redirect_uris** (первое значение) прописать в файл конфигурации .env. 

```
echo API_PORT = 3000 > .env
echo REDIS_HOST = localhost >> .env
echo REDIS_PORT = 6379 >> .env
echo REDIS_DB = 0 >> .env
echo REDIS_QUEUE_NAME = GMAILER >> .env
echo REDIS_QUEUE_RETRIES = 5 >> .env
echo MONGO_HOST = localhost >> .env
echo MONGO_PORT = 27017 >> .env
echo MONGO_DB_NAME = gmailer >> .env
echo MONGO_COLLECTION_NAME = mailbox >> .env
echo GMAIL_USER = your@gmail.com >> .env
echo GAPI_CLIENT_ID =  your-client-id >> .env
echo GAPI_CLIENT_SECRET =  your-secret >> .env
echo GAPI_CLIENT_REDIRECT_URI = your-redirect-uri >> .env
```

## Запуск

#### Gmailer API
```
node gmailer-api.js
```
#### Обработчик очереди рассылки
в отдельном окне терминала
```
node queue-worker.js
```

## Спецификация API

Добавить письмо в очередь рассылки

### Request
**Method:** POST

**URL:** /email

**Content-Type:** application/json

**Body:**
```
{
    "message": "Test",
    "subject": "Test",
    "from": "sender@gmail.com",
    "to": "recipient@gmail.com"
}
```

### Response
**Content-Type:** application/json

**Status:** 201 

**Body:**
```
{
    "code": 201,
    "message": "Письмо добавлено в очередь рассылки."
}
```

**Status:** 400

**Body:**
```
{
    "code": 400,
    "message": "Validation failed. Missing required property: to"
}
```

**Status:** 404

**Body:**
```
{
    "code": 404,
    "message": "Not found. Requested resource: POST /emailww"
}
```

**Status:** 500

**Body:**
```
{
    "code": 500,
    "message": "Internal server error."
}
```
