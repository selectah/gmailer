const tv4 = require('tv4');

exports.processError = function (error, request, response, next) {
  console.error(request.method + ' ' + request.originalUrl, error);

  if (error.code) {
    response.status(error.code).json(error);
  }
  else {
    response.status(500).json({code: 500, message: 'Internal server error.'});
  }
};

exports.validateRequest = function(request, response, next){
  tv4.validate(request.body, require(__dirname + '/../schemas/POST_email.json'));
  if(tv4.error){
    const validationError = {
      code: 400,
      message: 'Validation failed. ' + tv4.error.message
    };
    next(validationError);
  } else {
    next();
  }
};