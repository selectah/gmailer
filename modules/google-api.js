require('dotenv').config({path: __dirname + '/../.env'});
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const TOKEN_PATH = __dirname + '/../auth/token.json';
const SCOPES = [
  'https://www.googleapis.com/auth/gmail.send'
];

function authorize(complete) {
  const oAuth2Client = new google.auth.OAuth2(
    process.env['GAPI_CLIENT_ID'],
    process.env['GAPI_CLIENT_SECRET'],
    process.env['GAPI_CLIENT_REDIRECT_URI']
  );
  getToken(oAuth2Client, (error, token) => {
    if (!error) {
      oAuth2Client.setCredentials(token);
    }
    complete(error, oAuth2Client);
  });
}

function getToken(oAuth2Client, complete) {
  fs.readFile(TOKEN_PATH, {encoding: 'utf8'}, (error, token) => {
    if (error) {
      getNewToken(oAuth2Client, complete);
    } else {
      try {
        token = JSON.parse(token);
        complete(null, token);
      } catch (error) {
        complete(`Failed to parse ${TOKEN_PATH}. ${error.toString()}`);
      }
    }
  });
}

function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (error, token) => {
      if (error) {
        callback('Error retrieving access token. ' + error.toString());
      } else {
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (error) => {
          if (!error) {
            console.log('Token stored to', TOKEN_PATH);
          }
          callback(error, token);
        });
      }
    });
  });
}

exports.authorize = authorize;
