require('dotenv').config({path: __dirname + '/.env'});
const async = require('async');
const MongoClient = require('mongodb').MongoClient;
const mongoURL = `mongodb://${process.env['MONGO_HOST']}:${process.env['MONGO_PORT']}/${process.env['MONGO_DB_NAME']}`;
let mongoDB;

const redis = require('redis');
const redisClient = redis.createClient(process.env['REDIS_PORT'], process.env['REDIS_HOST'], {db: process.env['REDIS_DB']});

const beeQueue = require('bee-queue');

const mailQueue = new beeQueue(process.env['REDIS_QUEUE_NAME'],
  {
    redis: redisClient,
    isWorker: false,
    removeOnSuccess: true,
    removeOnFailure: true
  }
);

mailQueue.on('error', (error) => {
  console.log(`${process.env['REDIS_QUEUE_NAME']} queue error: ${err.message}`);
  process.exit(-1);
});

MongoClient.connect(mongoURL, {useUnifiedTopology: true}, (error, client) => {
  if(!error){
    mongoDB = client.db(process.env['MONGO_DB_NAME']);
  } else {
    console.error('Failed to connect to MongoDB. ' + error.toString());
    process.exit(-1);
  }
});

exports.saveMail = (mail, complete) => {
  mail.status = 'waiting';
  async.parallel(
    [
      (done) => {
        mongoDB.collection(process.env['MONGO_COLLECTION_NAME']).insertOne(mail, (error, result) => {
          done(error, result);
        });
      },
      (done) => {
        mailQueue.createJob(mail)
          .retries(process.env['REDIS_QUEUE_RETRIES'])
          .save(done);
      },
    ],
    (error, result) => {
      complete(error);
    }
  );
};
